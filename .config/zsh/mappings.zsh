bindkey -e

bindkey '^[[H' beginning-of-line                    # Home key
bindkey '^[[F' end-of-line                          # End key

bindkey '^[[2~' overwrite-mode                      # Insert key
bindkey '^[[3~' delete-char                         # Delete key

bindkey '^[[C'  forward-char                        # Right key
bindkey '^[[D'  backward-char                       # Left key

bindkey '^[[5~' history-beginning-search-backward   # Page up key
bindkey '^[[6~' history-beginning-search-forward    # Page down key

# Navigate words with ctrl+arrow keys
bindkey '^[Oc' forward-word         # ctrl - ->
bindkey '^[[1;5C' forward-word      # ctrl - ->

bindkey '^[Od' backward-word        # ctrl - <-
bindkey '^[[1;5D' backward-word     # ctrl - <-

bindkey '^H' backward-kill-word     # delete previous word with ctrl+backspace
bindkey '^[[Z' undo                 # Shift+tab undo last action

# if [[ "$terminfo[kcuu1]" != "" ]]; then
#     bindkey "$terminfo[kcuu1]" history-substring-search-up
# fi
#
# if [[ "$terminfo[kcud1]" != "" ]]; then
#     bindkey "$terminfo[kcud1]" history-substring-search-down
# fi
# make arrow keys behave in selection menus:
bindkey              '^I' menu-select
bindkey "$terminfo[kcbt]" menu-select
bindkey -M menuselect  "$terminfo[kcbt]" reverse-menu-complete
bindkey -M menuselect  '^I'    menu-complete
bindkey -M menuselect  '^M'    .accept-line
bindkey -M menuselect  '^[[C'  .forward-char  '^[OC' .forward-char
bindkey -M menuselect  '^[[D'  .backward-char '^[OD' .backward-char
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey '^R' .history-incremental-search-backward
bindkey '^S' .history-incremental-search-forward
