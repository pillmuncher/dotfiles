# Completion settings
# Case insensitive tab completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
# Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
# Automatically find new executables in path
zstyle ':completion:*' rehash true

# Speed up completions
# Accept exact matches without further selection
zstyle ':completion:*' accept-exact '*(N)'
# Set the cache path for completion data
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME/zsh"
# Enable caching of completion results
zstyle ':completion:*' use-cache on

# all Tab widgets
zstyle ':autocomplete:*complete*:*' insert-unambiguous yes

# all history widgets
zstyle ':autocomplete:*history*:*' insert-unambiguous yes

# ^S
zstyle ':autocomplete:menu-search:*' insert-unambiguous yes
