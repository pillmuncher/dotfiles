# History section:
export SAVEHIST=20000
export HISTSIZE="$SAVEHIST"
export HISTFILE="$XDG_STATE_HOME/zsh/history"
fc -R
