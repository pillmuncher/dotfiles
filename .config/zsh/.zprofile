# my editor
export VISUAL=nvim
export EDITOR="$VISUAL"

# personal language settings:

LANG=en_US.utf8
LC_NUMERIC=de_DE.UTF-8
LC_TIME=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
#LC_NAME=de_DE.utf8
LC_ADDRESS=de_DE.utf8
LC_TELEPHONE=de_DE.utf8
LC_MEASUREMENT=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.utf8

# make output of ls look nice
export LS_COLORS="*.old=38;5;243:*.tmp=38;5;243:$LS_COLORS"

# Color man pages
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

# fix the 1m...0m problem in man pages
export GROFF_NO_SGR=1

# show colored man pages
# export MANPAGER="sh -c 'col -bx | bat -l man -p' -"
export MANPAGER="nvim +Man!"

# replace less
export PAGER=bat

# but make git/delta use less
export DELTA_PAGER=less

# Directories:

export ANDROID_HOME="$XDG_DATA_HOME/android"
export ANDROID_USER_HOME="$XDG_DATA_HOME/android"

export ANTIDOTE_HOME="$XDG_CONFIG_HOME/antidote"

export ANT_HOME="$XDG_DATA_HOME/apache-ant-1.10.15"

export CARGO_HOME="$XDG_DATA_HOME/cargo"

export DOTNET_ROOT="$XDG_DATA_HOME/dotnet"
export DOTNET_CLI_HOME="$DOTNET_ROOT"
export NUGET_PACKAGES="$XDG_CACHE_HOME/NuGetPackages"
export OMNISHARPHOME="$XDG_CONFIG_HOME/omnisharp"

export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"

export DVDCSS_CACHE="$XDG_CACHE_HOME/dvdcss"

export FZF_BASE="$XDG_CONFIG_HOME/fzf"

export GNUPGHOME="$XDG_DATA_HOME/gnupg"

export GOPATH="$XDG_DATA_HOME/go"

export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"

export JULIAUP_DEPOT_PATH="$XDG_DATA_HOME/julia"
export JULIA_DEPOT_PATH="$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"

export LEIN_HOME="$XDG_DATA_HOME/lein"

export npm_config_userconfig="$XDG_CONFIG_HOME/npm/npmrc"
export NODE_REPL_HISTORY="$XDG_DATA_HOME/npm/history"
export NPM_CONFIG_CACHE="$XDG_CACHE_HOME"/npm
export NPM_CONFIG_INIT_MODULE="$XDG_CONFIG_HOME"/npm/config/npm-init.js
export NPM_CONFIG_TMP="$XDG_RUNTIME_DIR"/npm
export NVM_DIR="$XDG_DATA_HOME/nvm"

export PARALLEL_HOME="$XDG_CONFIG_HOME/parallel"

export PERL_BASE="$XDG_DATA_HOME/perl5"
export PERL5LIB="$PERL_BASE/lib/perl5"
export PERLOPT="-Mlocal::lib=$PERL_BASE"
export PERL_CPANM_HOME="$XDG_CACHE_HOME/cpanm"
export PERL_LOCAL_LIB="$PERL_BASE"
export PERL_LOCAL_LIB_ROOT="$PERL_BASE"
export PERL_MB_OPT="--install_base $PERL_BASE"
export PERL_MM_OPT="INSTALL_BASE=$PERL_BASE"

export PGPASSFILE="$XDG_CONFIG_HOME/pg/pgpass"
export PSQL_HISTORY="$XDG_STATE_HOME/psql_history"

export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export PYTHON_HISTORY="$XDG_STATE_HOME/python/history"
export WORKON_HOME="$XDG_DATA_HOME/virtualenvs"

export RECOLL_CONFDIR="$XDG_CONFIG_HOME/recoll"

export TK_LIBRARY="/usr/lib64/libtk8.6.so"
export TCL_LIBRARY="/usr/lib64/libtcl8.6.so"

export WINEPREFIX="$XDG_DATA_HOME/wine"

export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"

# This doesn't work since it's hard-coded in LightDM to create it in $HOME.
# I leave it in just in case LightDM fixes this.
export ERRFILE="$XDG_CACHE_HOME/X11/xsession-errors"

export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java


PATH="/usr/lib64/openjdk-17/bin:$PATH"
PATH="$XDG_DATA_HOME/npm/bin:$PATH"
PATH="$XDG_DATA_HOME/gem/ruby/3.2.0/bin:$PATH"
PATH="$PERL_LOCAL_LIB/bin:$PATH"
PATH="$DOTNET_ROOT:$PATH"
PATH="$HOME/Applications:$PATH"
export PATH

source "$XDG_CONFIG_HOME/aliases"

# start ssh-agent if it's not already running:
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
  eval $(ssh-agent -s)
fi
