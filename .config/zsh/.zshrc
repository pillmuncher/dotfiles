echo "$USER@$HOST  $(uname -srm) $(lsb_release -rcs)"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

autoload -U cdr
autoload -U colors && colors
autoload -U compinit && compinit -d "$XDG_CACHE_HOME/zsh/zcompdump"
autoload -U promptinit && promptinit
autoload -U zcalc
autoload -U zmv

# Antidote settings
zstyle ':antidote:compatibility-mode' 'antibody'
zstyle ':antidote:bundle' use-friendly-names on
zstyle ':antidote:load' sequential false

if [[ -e "$XDG_DATA_HOME/antidote/antidote.zsh" ]]; then
    source "$XDG_DATA_HOME/antidote/antidote.zsh"
else
    source /usr/share/antidote/antidote.zsh
fi
antidote load "${ZDOTDIR:-$HOME}/.zsh_plugins.txt"

source "$XDG_CONFIG_HOME/aliases"
precmd() { source "$XDG_CONFIG_HOME/aliases" }

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh
