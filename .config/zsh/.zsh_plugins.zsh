fpath+=( $HOME/.config/antidote/djui/alias-tips )
source $HOME/.config/antidote/djui/alias-tips/alias-tips.plugin.zsh
fpath+=( $HOME/.config/antidote/mattmc3/zephyr/plugins/confd )
source $HOME/.config/antidote/mattmc3/zephyr/plugins/confd/confd.plugin.zsh
fpath+=( $HOME/.config/antidote/romkatv/powerlevel10k )
source $HOME/.config/antidote/romkatv/powerlevel10k/powerlevel10k.zsh-theme
source $HOME/.config/antidote/romkatv/powerlevel10k/powerlevel9k.zsh-theme
fpath+=( $HOME/.config/antidote/zsh-users/zsh-history-substring-search )
source $HOME/.config/antidote/zsh-users/zsh-history-substring-search/zsh-history-substring-search.plugin.zsh
fpath+=( $HOME/.config/antidote/zsh-users/zsh-syntax-highlighting )
source $HOME/.config/antidote/zsh-users/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
fpath+=( $HOME/.config/antidote/zdharma-continuum/fast-syntax-highlighting )
source $HOME/.config/antidote/zdharma-continuum/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
fpath+=( $HOME/.config/antidote/zsh-users/zsh-autosuggestions )
source $HOME/.config/antidote/zsh-users/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/async_prompt.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/bzr.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/clipboard.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/cli.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/compfix.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/completion.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/correction.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/diagnostics.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/directories.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/functions.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/git.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/grep.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/history.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/key-bindings.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/misc.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/nvm.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/prompt_info_functions.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/spectrum.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/termsupport.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/theme-and-appearance.zsh
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/lib/vcs_info.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/aliases )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/aliases/aliases.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/bgnotify )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/bgnotify/bgnotify.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/copybuffer )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/copybuffer/copybuffer.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/copypath )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/copypath/copypath.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/direnv )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/direnv/direnv.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/docker-compose )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/docker-compose/docker-compose.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/dotnet )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/dotnet/dotnet.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/extract )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/extract/extract.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/fancy-ctrl-z )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/fancy-ctrl-z/fancy-ctrl-z.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/fzf )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/fzf/fzf.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/gitfast )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/gitfast/gitfast.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/gitignore )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/gitignore/gitignore.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/gpg-agent )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/gpg-agent/gpg-agent.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/history )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/history/history.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/isodate )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/isodate/isodate.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/jsontools )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/jsontools/jsontools.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/kitty )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/kitty/kitty.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/pip )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/pip/pip.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/postgres )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/postgres/postgres.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/pre-commit )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/pre-commit/pre-commit.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/profiles )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/profiles/profiles.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/python )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/python/python.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/qrcode )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/qrcode/qrcode.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/rsync )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/rsync/rsync.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/safe-paste )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/safe-paste/safe-paste.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/ssh )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/ssh/ssh.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/sudo )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/sudo/sudo.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/systemd )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/systemd/systemd.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/universalarchive )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/universalarchive/universalarchive.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/urltools )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/urltools/urltools.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/zsh-navigation-tools )
source $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/zsh-navigation-tools/zsh-navigation-tools.plugin.zsh
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/docker )
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/gh )
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/lein )
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/pdm )
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/rclone )
fpath+=( $HOME/.config/antidote/ohmyzsh/ohmyzsh/plugins/ripgrep )
fpath+=( $HOME/.config/antidote/marlonrichert/zsh-autocomplete )
source $HOME/.config/antidote/marlonrichert/zsh-autocomplete/zsh-autocomplete.plugin.zsh
