setopt APPEND_HISTORY        # Immediately append history instead of overwriting
setopt AUTO_CD               # If only a directory path is entered, cd there.
setopt AUTO_PUSHD            # Automatically add . to directory memory stack
setopt CORRECT               # Auto correct mistakes
setopt EXTENDED_GLOB         # Extended globbing. Allows using regular expressions with *
setopt EXTENDED_HISTORY
setopt HIST_IGNORE_ALL_DUPS  # If a new command is a duplicate, remove the older one
setopt NO_BEEP               # No beep
setopt NO_CASE_GLOB          # Case insensitive globbing
setopt NO_CHECK_JOBS         # Don't warn about running processes when exiting
setopt NO_BAD_PATTERN        # Prevent `zsh: bad pattern: #`
setopt NUMERIC_GLOB_SORT     # Sort filenames numerically when it makes sense
setopt PROMPT_SUBST          # Enable prompt string expansion
setopt PUSHD_IGNORE_DUPS     # Don't push directory to stack if it's already there
setopt PUSHD_MINUS           # Allow `cd -` to switch between the current and previous directories
setopt PUSHD_SILENT          # Silent pushd/poppd (no output when changing directories)
setopt PUSHD_TO_HOME         # Pushd to home directory
setopt RC_EXPAND_PARAM       # Array expansion with parameters
setopt SHARE_HISTORY         # Share command history between all sessions
