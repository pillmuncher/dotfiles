alias cp='cp -i'     # Confirm before overwriting something
alias mv='mv -i'
alias rm='rm -i'
alias rd='rmdir'
alias md='mkdir'

alias gitu='git add . && git commit && git push'

# Default to human readable figures
alias free='free -h'
alias df='df -h'
alias du='du -h'

alias less='less -r'       # raw control characters
alias o='less'             # raw control characters
alias whence='type -a'     # where, of a sort
alias grep='grep --color'  # show differences in colour

# Some shortcuts for different directory listings
alias ls='lsd -vhF --color=always --group-directories-first'
alias la='ls -A'           # all but . and ..
alias ll='ls -l'           # long list
alias lla='ls -lA'         # long list, all but . and ..

alias tree='tree -vCF --dirsfirst'

alias -- +='pushd .'
alias -- -='popd 2>/dev/null'

alias cd..='cd ..'
alias cd...='cd ../..'

alias vi='nvim'
alias vim='nvim'
alias gvi='neovide --'
alias gvim='neovide --'

alias adb='HOME="$XDG_DATA_HOME"/android adb'
alias ccat='pygmentize -g'
alias dff='df -hHT -x tmpfs -x devtmpfs | (read -r; printf "%s\n" "$REPLY"; sort -k 1)'
alias dotfiles='/usr/bin/git --git-dir=$XDG_DATA_HOME/dotfiles/ --work-tree=$HOME'
alias ea='extract'
alias keychain='keychain --dir "$XDG_RUNTIME_DIR"/keychain --absolute'
alias mvn="mvn -gs $XDG_CONFIG_HOME/maven/settings.xml"
alias twine='twine --config-file $XDG_CONFIG_HOME/.pypirc'
alias udevinfo='/sbin/udevadm info'
alias wget='wget --hsts-file=$XDG_DATA_HOME/wget-hsts'
alias xclip='xclip -selection clipboard'
